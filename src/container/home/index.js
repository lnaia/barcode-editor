import React from 'react';
import { Editor } from '../../components/editor';
import { Header } from '../../components/header';
import { Footer } from '../../components/footer';
import styles from './index.module.css';

const App = () => {
  const { container } = styles;
  return (
    <div className={container}>
      <Header/>
      <Editor/>
      <Footer/>
    </div>
  );
};

export default App;
