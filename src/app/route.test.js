import React from 'react';
import { shallow } from 'enzyme';
import { AppRouter } from './route';

describe('AppRouter', () => {
  it('renders with the default values', () => {
    const component = shallow(<AppRouter/>);
    const routes = component.find('Route');
    expect(routes.at(0).prop('component')).toBeDefined();
    expect(routes.at(0).prop('path')).toBeUndefined();
    expect(routes.at(1).prop('path')).toBe('/');
    expect(routes.at(1).prop('component')).toBeDefined();
  });
});
