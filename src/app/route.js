import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Home from '../container/home';
import { Layout } from '../components/layout';

export const AppRouter = () => {
  return (
    <Router>
      <Route component={Layout}>
        <Route path="/" exact component={Home}/>
      </Route>
    </Router>
  );
};
