import * as constants from './constants';

describe('constants are defined', () => {
  const expectedConstants = [
    'ONE',
    'TWO',
    'THREE',
    'FOUR',
    'FIVE',
    'SIX',
    'SEVEN',
    'EIGHT',
    'NINE',
    'ZERO',
    'KEY_NUMBER_START',
    'KEY_NUMBER_FINISH',
    'KEY_NUMPAD_START',
    'KEY_NUMPAD_FINISH',
    'KEY_DELETE',
    'KEY_BACKSPACE',
    'KEY_LEFT_ARROW',
    'KEY_RIGHT_ARROW',
    'KEY_HOME',
    'KEY_END',
    'PRIMARY_COLOR',
    'SECONDARY_COLOR',
    'HIDE_NUMBERS',
    'STORAGE_CACHE_KEY',
    'EDITOR_STORAGE_KEY',
    'MAX_DIGIT_INPUT'
  ];

  test('constants are all accounted for', () =>
    expect(expectedConstants.length).toBe(Object.keys(constants).length));

  expectedConstants.forEach(constant =>
    test(`expects ${constant} to be defined`, () =>
      expect(constants[constant]).toBeDefined()));
});
