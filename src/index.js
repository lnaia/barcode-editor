import React from 'react';
import ReactDOM from 'react-dom';
import './styles/theme.css';
import { AppRouter } from './app/route';
import * as serviceWorker from './utils/service-worker';

ReactDOM.render(<AppRouter/>, document.getElementById('root'));

// If you want your home to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
