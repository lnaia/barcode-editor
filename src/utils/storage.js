import { STORAGE_CACHE_KEY } from '../app/constants';

export const keyWrapper = ({ key }) =>
  `${STORAGE_CACHE_KEY}_${key}`;

export const set = ({
  key,
  value,
  storage = window.localStorage
}) => {
  try {
    storage.setItem(keyWrapper({ key }), JSON.stringify(value));
  } catch (err) {
    // ignore write errors
  }
};

export const get = ({
  key,
  storage = window.localStorage
}) => {
  try {
    const data = storage.getItem(keyWrapper({ key }));
    return JSON.parse(data);
  } catch (err) {
    // ignore read errors
  }
};
