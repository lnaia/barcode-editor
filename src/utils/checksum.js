export const isEven = i => i % 2 === 0;

export const isOdd = i => !isEven(i);

export const oddPositions = digits =>
  digits.filter((v, i) => isOdd(i + 1));

export const evenPositions = digits =>
  digits.filter((v, i) => isEven(i + 1));

export const sum = digits =>
  digits.reduce((res, v) => res + v, 0);

export const step1 = (digits) =>
  3 * sum(oddPositions(digits));

export const step2 = (digits) =>
  sum(evenPositions(digits));

export const calculate = ({ digits }) => {
  const intermediate = (step1(digits) + step2(digits)) % 10;
  const remainder = intermediate % 10;
  return remainder === 0 ? 0 : 10 - remainder;
};

