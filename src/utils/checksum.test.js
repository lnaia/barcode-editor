import {
  isEven,
  isOdd,
  oddPositions,
  evenPositions,
  sum,
  step1,
  step2,
  calculate
} from './checksum';

describe('checksum', () => {
  it('isEven', () => {
    expect(isEven(2)).toBe(true);
    expect(isEven(1)).toBe(false);
  });

  it('isOdd', () => {
    expect(isOdd(1)).toBe(true);
    expect(isOdd(2)).toBe(false);
  });

  it('oddPositions', () => {
    expect(oddPositions([1, 2, 4, 3])).toEqual([1, 4]);
    expect(oddPositions([2])).toEqual([2]);
    expect(oddPositions([2, 3])).toEqual([2]);
  });

  it('evenPositions', () => {
    expect(evenPositions([1, 2, 4, 3])).toEqual([2, 3]);
    expect(evenPositions([2])).toEqual([]);
    expect(evenPositions([2, 3])).toEqual([3]);
  });

  it('sum', () => {
    expect(sum([])).toEqual(0);
    expect(sum([1])).toEqual(1);
    expect(sum([1, 2])).toEqual(3);
  });

  it('step1', () => {
    expect(step1([])).toEqual(0);
    expect(step1([1])).toEqual(3);
    expect(step1([5, 4, 8, 9, 8, 5, 0, 3, 5, 4])).toEqual(78);
  });

  it('step2', () => {
    expect(step2([])).toEqual(0);
    expect(step2([1])).toEqual(0);
    expect(step2([5, 4, 8, 9, 8, 5, 0, 3, 5, 4])).toEqual(25);
  });

  it('calculate', () => {
    expect(calculate({ digits: [] })).toEqual(0);
    expect(calculate({ digits: [1] })).toEqual(7);
    expect(calculate({ digits: [5, 4, 8, 9, 8, 5, 0, 3, 5, 4] })).toEqual(7);
  });
});
