import { STORAGE_CACHE_KEY } from '../app/constants';
import { stub, assert } from 'sinon';
import {
  keyWrapper,
  get,
  set
} from './storage';

describe('storage', () => {
  it('keyWrapper', () => {
    const key = 'sample';
    expect(keyWrapper({ key })).toBe(`${STORAGE_CACHE_KEY}_${key}`);
  });

  it('set', () => {
    const setItem = stub();
    const storage = { setItem };
    const key = 'sampleKey';
    const value = { sample: 'value' };
    set({ key, value, storage });
    assert.calledWith(setItem, `${STORAGE_CACHE_KEY}_${key}`, JSON.stringify(value));
  });

  it('get', () => {
    const key = 'sampleKey';
    const value = { sample: 'value' };
    const getItem = stub().returns(JSON.stringify(value));
    const storage = { getItem };
    const result = get({ key, value, storage });
    assert.calledWith(getItem, `${STORAGE_CACHE_KEY}_${key}`);
    expect(result).toEqual(JSON.parse(JSON.stringify(value)));
  });
});
