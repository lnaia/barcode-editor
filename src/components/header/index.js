import React from 'react';
import styles from './index.module.css';
import classNames from 'classnames';

export const Header = () => {
  const { container } = styles;
  const headerClass = classNames('primary-bg-color', container);
  return (
    <h1 className={headerClass}>
      Barcode Editor
    </h1>
  );
};
