import React from 'react';
import styles from './index.module.css';

export const Footer = () => {
  const { footer } = styles;
  return (
    <div className={footer}>
      You can find this code at: <a href="https://gitlab.com/lnaia/barcode-editor">gitlab.com/lnaia/barcode-editor</a>
    </div>
  );
};
