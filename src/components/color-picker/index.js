import React from 'react';
import PropTypes from 'prop-types';

export const ColorPicker = ({
  value,
  onChange
}) => {
  return (

    <div className="input-group input-group-sm">
      <div className="input-group-prepend">
        <div className="input-group-text">Primary Color</div>
      </div>
      <input
        type='color'
        className="form-control"
        value={value}
        onChange={(event) => onChange(event.target.value)}
      />
    </div>
  );
};

ColorPicker.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string
};

ColorPicker.defaultProps = {
  value: '#fff'
};
