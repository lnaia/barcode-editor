import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import { stub, assert } from 'sinon';
import { ColorPicker } from './index';

describe('ColorPicker', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    const props = { onChange: () => {} };
    ReactDOM.render(<ColorPicker {...props}/>, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('calls onChange with the correct value', () => {
    const onChange = stub();
    const props = { onChange };
    const value = '[value]';
    const component = shallow(<ColorPicker {...props}/>);
    component.find('input').simulate('change', { target: { value } });
    assert.calledWith(onChange, value);
  });

  it('renders with the default values', () => {
    const onChange = stub();
    const props = { onChange };
    const component = shallow(<ColorPicker {...props}/>);
    const value = component.find('input').prop('value');
    expect(value).toBe('#fff');
  });
});
