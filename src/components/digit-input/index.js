import PropTypes from 'prop-types';
import React from 'react';
import {
  KEY_NUMBER_START,
  KEY_NUMBER_FINISH,
  KEY_NUMPAD_START,
  KEY_NUMPAD_FINISH,
  KEY_DELETE,
  KEY_BACKSPACE,
  KEY_LEFT_ARROW,
  KEY_RIGHT_ARROW,
  KEY_HOME,
  KEY_END,
  MAX_DIGIT_INPUT
} from '../../app/constants';

export const parseText = (event) =>
  `${event.target.value}`
    .split('')
    .map(i => +i);

export const isKeyNumber = ({ keyCode }) => {
  const number = keyCode >= KEY_NUMBER_START && keyCode <= KEY_NUMBER_FINISH;
  const numPad = keyCode >= KEY_NUMPAD_START && keyCode <= KEY_NUMPAD_FINISH;
  const navigationKeys = keyCode === KEY_BACKSPACE
    || keyCode === KEY_DELETE
    || keyCode === KEY_LEFT_ARROW
    || keyCode === KEY_RIGHT_ARROW
    || keyCode === KEY_END
    || keyCode === KEY_HOME;

  return number || numPad || navigationKeys;
};

export const DigitInput = ({
  onChange
}) => {
  return (
    <div className="input-group input-group-sm">
      <div className="input-group-prepend">
        <div className="input-group-text" id="typeADigit">Digits</div>
      </div>
      <input
        type="text"
        className="form-control"
        placeholder="Type a digit"
        aria-label="Type a digit"
        aria-describedby="typeADigit"
        maxLength={MAX_DIGIT_INPUT}
        onKeyDown={(event) => {
          if (!isKeyNumber({ keyCode: event.keyCode })) {
            event.preventDefault();
          }
        }}
        onChange={event => onChange(parseText(event))}
      />
    </div>
  );
};

DigitInput.propTypes = {
  onChange: PropTypes.func.isRequired
};
