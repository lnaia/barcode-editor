import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import { stub, assert } from 'sinon';
import { DigitInput, parseText, isKeyNumber } from './index';
import {
  KEY_DELETE,
  KEY_BACKSPACE,
  KEY_LEFT_ARROW,
  KEY_RIGHT_ARROW,
  KEY_HOME,
  KEY_END,
  MAX_DIGIT_INPUT
} from '../../app/constants';

describe('DigitInput', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    const props = { onChange: () => {} };
    ReactDOM.render(<DigitInput {...props}/>, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('calls onChange with the correct value', () => {
    const onChange = stub();
    const props = { onChange };
    const value = 0;
    const component = shallow(<DigitInput {...props}/>);
    component.find('input').simulate('change', { target: { value } });
    assert.calledWith(onChange, [value]);
  });

  it('blocks keypress on letters', () => {
    const onChange = stub();
    const preventDefault = stub();
    const props = { onChange };
    const component = shallow(<DigitInput {...props}/>);
    component.find('input').simulate('keydown', { keyCode: 65, preventDefault });
    assert.called(preventDefault);
  });

  it('allows keypress on numbers', () => {
    const onChange = stub();
    const preventDefault = stub();
    const props = { onChange };
    const component = shallow(<DigitInput {...props}/>);
    component.find('input').simulate('keydown', { keyCode: 48, preventDefault });
    assert.notCalled(preventDefault);
  });

  it('limits input to MAX_DIGIT_INPUT max length', () => {
    const onChange = stub();
    const props = { onChange };
    const component = shallow(<DigitInput {...props}/>);
    const value = component.find('input').prop('maxLength');
    expect(value).toBe(MAX_DIGIT_INPUT);
  });

  it('parseText splits a number into an array of digits', () => {
    const result = parseText({ target: { value: '0123456789' } });
    expect(result).toEqual([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
  });

  describe('isKeyNumber identifies numbers and navigation keys', () => {
    const range = (a, b) => Array.from(new Array((b + 1 - a)), (x, i) => i + a);
    const letters = range(65, 90);
    const digits = range(48, 57);
    const numPad = range(96, 105);
    const navigationKeys = [
      KEY_BACKSPACE,
      KEY_LEFT_ARROW,
      KEY_RIGHT_ARROW,
      KEY_HOME,
      KEY_END,
      KEY_DELETE
    ];

    digits.forEach((keyCode) =>
      it(`identifies digits, w/ keyCode: ${keyCode}`, () =>
        expect(isKeyNumber({ keyCode })).toBe(true)));

    numPad.forEach((keyCode) =>
      it(`identifies numPad, w/ keyCode: ${keyCode}`, () =>
        expect(isKeyNumber({ keyCode })).toBe(true)));

    navigationKeys.forEach((keyCode) =>
      it(`identifies navigationKeys, w/ keyCode: ${keyCode}`, () =>
        expect(isKeyNumber({ keyCode })).toBe(true)));

    letters.forEach((keyCode) =>
      it(`identifies letters, w/ keyCode: ${keyCode}`, () =>
        expect(isKeyNumber({ keyCode })).toBe(false)));

  });
});
