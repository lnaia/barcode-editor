import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './index.module.css';

export const HideNumbers = ({
  value,
  onChange
}) => {
  const { container } = styles;
  const hideNumbersClass = classNames(
    'form-check',
    'form-check-inline',
    'hide-numbers',
    container
  );

  return (
    <div className={hideNumbersClass}>
      <input
        className="form-check-input"
        id={'hideNumbers'}
        type={'checkbox'}
        onChange={(event) => onChange(event.target.checked)}
        checked={value}
      />
      <label className="form-check-label" htmlFor="hideNumbers">Hide Numbers</label>
    </div>
  );
};

HideNumbers.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.bool
};

HideNumbers.defaultProps = {
  value: true
};
