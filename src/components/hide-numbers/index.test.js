import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import { stub, assert } from 'sinon';
import { HideNumbers } from './index';

describe('HideNumbers', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    const props = { onChange: () => {} };
    ReactDOM.render(<HideNumbers {...props}/>, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('calls onChange with the correct value', () => {
    const onChange = stub();
    const props = { onChange };
    const checked = true;
    const component = shallow(<HideNumbers {...props}/>);
    component.find('input').simulate('change', { target: { checked } });
    assert.calledWith(onChange, checked);
  });

  it('renders with the default values', () => {
    const onChange = stub();
    const props = { onChange };
    const component = shallow(<HideNumbers {...props}/>);
    const value = component.find('input').prop('checked');
    expect(value).toBe(true);
  });
});
