import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import { Editor } from './index';

describe('Editor', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Editor/>, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('show dependent components', () => {
    const component = shallow(<Editor/>);
    expect(component.find('DataInput').length).toBe(1);
    expect(component.find('Display').length).toBe(1);
  });

  it('dataInput component attributes are all defined', () => {
    const component = shallow(<Editor/>);
    const dataInput = component.find('DataInput');
    expect(dataInput.prop('defaultPrimaryColor')).toBeDefined();
    expect(dataInput.prop('defaultSecondaryColor')).toBeDefined();
    expect(dataInput.prop('defaultHideNumbers')).toBeDefined();
    expect(dataInput.prop('onChangeDigitInput')).toBeDefined();
    expect(dataInput.prop('onChangePrimaryColor')).toBeDefined();
    expect(dataInput.prop('onChangeSecondaryColor')).toBeDefined();
    expect(dataInput.prop('onChangeHideNumbers')).toBeDefined();
  });

  it('Display component attributes are all defined', () => {
    const component = shallow(<Editor/>);
    const dataInput = component.find('Display');
    expect(dataInput.prop('hideNumbers')).toBeDefined();
    expect(dataInput.prop('digits')).toBeDefined();
    expect(dataInput.prop('primaryColor')).toBeDefined();
    expect(dataInput.prop('secondaryColor')).toBeDefined();
  });

  it('dataInput calls onChangeDigitInput', () => {
    const component = shallow(<Editor/>);
    const dataInput = component.find('DataInput');
    const digits = [1, 2, 3];
    dataInput.prop('onChangeDigitInput')(digits);

    const display = component.find('Display');
    expect(display.prop('digits')).toEqual(digits);
  });

  it('dataInput calls onChangePrimaryColor', () => {
    const component = shallow(<Editor/>);
    const dataInput = component.find('DataInput');
    const color = '#123';
    dataInput.prop('onChangePrimaryColor')(color);

    const display = component.find('Display');
    expect(display.prop('primaryColor')).toEqual(color);
  });

  it('dataInput calls onChangeSecondaryColor', () => {
    const component = shallow(<Editor/>);
    const dataInput = component.find('DataInput');
    const color = '#321';
    dataInput.prop('onChangeSecondaryColor')(color);

    const display = component.find('Display');
    expect(display.prop('secondaryColor')).toEqual(color);
  });

 it('dataInput calls onChangeHideNumbers', () => {
    const component = shallow(<Editor/>);
    const dataInput = component.find('DataInput');
    const hideNumbers = true;
    dataInput.prop('onChangeHideNumbers')(hideNumbers);

    const display = component.find('Display');
    expect(display.prop('hideNumbers')).toEqual(hideNumbers);
  });

});
