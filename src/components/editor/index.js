import React, { useState } from 'react';
import { DataInput } from '../data-input';
import { Display } from '../display';
import { set, get } from '../../utils/storage';
import {
  PRIMARY_COLOR,
  SECONDARY_COLOR,
  HIDE_NUMBERS,
  EDITOR_STORAGE_KEY
} from '../../app/constants';

export const Editor = () => {
  const initialState = get({ key: EDITOR_STORAGE_KEY }) || {
    digits: [],
    primaryColor: PRIMARY_COLOR,
    secondaryColor: SECONDARY_COLOR,
    hideNumbers: HIDE_NUMBERS
  };

  const [state, setState] = useState(initialState);
  const updateState = (state) => {
    set({
      key: EDITOR_STORAGE_KEY, value: {
        ...state,
        digits: [] // do not save the digits
      }
    });
    setState(state);
  };

  return (
    <div>
      <DataInput
        defaultPrimaryColor={state.primaryColor}
        defaultSecondaryColor={state.secondaryColor}
        defaultHideNumbers={state.hideNumbers}
        onChangeDigitInput={value => updateState({ ...state, digits: value })}
        onChangePrimaryColor={value => updateState({ ...state, primaryColor: value })}
        onChangeSecondaryColor={value => updateState({ ...state, secondaryColor: value })}
        onChangeHideNumbers={value => updateState({ ...state, hideNumbers: value })}
      />
      <hr/>
      <Display
        hideNumbers={state.hideNumbers}
        digits={state.digits}
        primaryColor={state.primaryColor}
        secondaryColor={state.secondaryColor}
      />
    </div>
  );
};
