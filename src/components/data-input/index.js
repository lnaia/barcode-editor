import classNames from 'classnames';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { ColorPicker } from '../color-picker';
import { DigitInput } from '../digit-input';
import { HideNumbers } from '../hide-numbers';
import './index.css';

export const DataInput = ({
  defaultPrimaryColor,
  defaultSecondaryColor,
  defaultHideNumbers,
  onChangeDigitInput,
  onChangePrimaryColor,
  onChangeSecondaryColor,
  onChangeHideNumbers
}) => {
  const [state, setState] = useState({
    primaryColor: defaultPrimaryColor,
    secondaryColor: defaultSecondaryColor,
    hideNumbers: defaultHideNumbers
  });
  const dataInputClass = classNames('primary-bg-color', 'DataInput');
  return (
    <div className={dataInputClass}>
      <DigitInput onChange={onChangeDigitInput}/>

      <ColorPicker
        value={state.primaryColor}
        onChange={(value) => {
          setState({ ...state, primaryColor: value });
          onChangePrimaryColor(value);
        }}
      />

      <ColorPicker
        value={state.secondaryColor}
        onChange={(value) => {
          setState({ ...state, secondaryColor: value });
          onChangeSecondaryColor(value);
        }}
      />

      <HideNumbers
        value={state.hideNumbers}
        onChange={(value) => {
          setState({ ...state, hideNumbers: value });
          onChangeHideNumbers(value);
        }}
      />
    </div>
  );
};

DataInput.propTypes = {
  defaultPrimaryColor: PropTypes.string.isRequired,
  defaultSecondaryColor: PropTypes.string.isRequired,
  onChangeDigitInput: PropTypes.func.isRequired,
  onChangeHideNumbers: PropTypes.func.isRequired,
  onChangePrimaryColor: PropTypes.func.isRequired,
  onChangeSecondaryColor: PropTypes.func.isRequired
};
