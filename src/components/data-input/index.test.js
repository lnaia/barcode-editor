import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import { assert, stub } from 'sinon';
import { ColorPicker } from '../color-picker';
import { DataInput } from './index';

describe('DataInput', () => {
  const props = {
    defaultPrimaryColor: '#ffff11',
    defaultSecondaryColor: '#11ffff',
    defaultHideNumbers: false,
    onChangeDigitInput: stub(),
    onChangeHideNumbers: stub(),
    onChangePrimaryColor: stub(),
    onChangeSecondaryColor: stub()
  };

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<DataInput {...props}/>, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('has dependant components', () => {
    const component = shallow(<DataInput {...props}/>);
    expect(component.find('DigitInput').length).toBe(1);
    expect(component.find('HideNumbers').length).toBe(1);
    expect(component.find('ColorPicker').length).toBe(2);
  });

  it('DigitInput component attributes are all defined', () => {
    const component = shallow(<DataInput {...props}/>);
    const DigitInput = component.find('DigitInput');
    expect(DigitInput.prop('onChange')).toBeDefined();
  });

  it('ColorPicker component attributes are all defined', () => {
    const component = shallow(<DataInput {...props}/>);
    const colorPickers = component.find('ColorPicker');
    expect(colorPickers.at(0).prop('value')).toBeDefined();
    expect(colorPickers.at(0).prop('value')).toBe(props.defaultPrimaryColor);
    expect(colorPickers.at(0).prop('onChange')).toBeDefined();

    expect(colorPickers.at(1).prop('value')).toBeDefined();
    expect(colorPickers.at(1).prop('value')).toBe(props.defaultSecondaryColor);
    expect(colorPickers.at(1).prop('onChange')).toBeDefined();
  });

  it('HideNumbers component attributes are all defined', () => {
    const component = shallow(<DataInput {...props}/>);
    const hideNumbers = component.find('HideNumbers');
    expect(hideNumbers.prop('value')).toBeDefined();
    expect(hideNumbers.prop('value')).toBe(props.defaultHideNumbers);
    expect(hideNumbers.prop('onChange')).toBeDefined();
  });

  it('DigitInput calls onChange with the correct value', () => {
    const onChangeDigitInput = stub();
    const newProps = { ...props, onChangeDigitInput };
    const value = '[value]';
    const component = shallow(<DataInput {...newProps}/>);
    const DigitInput = component.find('DigitInput');
    DigitInput.simulate('change', value);
    assert.calledWith(onChangeDigitInput, value);
  });

  it('ColorPicker calls onChange with the correct value', () => {
    const onChangePrimaryColor = stub();
    const newProps = { ...props, onChangePrimaryColor };
    const value = '[value]';
    const component = shallow(<DataInput {...newProps}/>);
    const ColorPicker = component.find('ColorPicker').at(0);
    ColorPicker.simulate('change', value);
    assert.calledWith(onChangePrimaryColor, value);
  });

  it('ColorPicker calls onChange with the correct value', () => {
    const onChangeSecondaryColor = stub();
    const newProps = { ...props, onChangeSecondaryColor };
    const value = '[value]';
    const component = shallow(<DataInput {...newProps}/>);
    const ColorPicker = component.find('ColorPicker').at(1);
    ColorPicker.simulate('change', value);
    assert.calledWith(onChangeSecondaryColor, value);
  });

  it('HideNumbers calls onChange with the correct value', () => {
    const onChangeHideNumbers = stub();
    const newProps = { ...props, onChangeHideNumbers };
    const value = false;
    const component = shallow(<DataInput {...newProps}/>);
    const HideNumbers = component.find('HideNumbers');
    HideNumbers.simulate('change', value);
    assert.calledWith(onChangeHideNumbers, value);
  });
});
