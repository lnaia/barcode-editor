import PropTypes from 'prop-types';
import React from 'react';
import styles from './index.module.css';
import {
  ZERO,
  ONE,
  TWO,
  THREE,
  FOUR,
  FIVE,
  SIX,
  SEVEN,
  EIGHT,
  NINE
} from '../../app/constants';

export const dimTpl = ({ w, h }) => ({ width: `${w}px`, height: `${h}px` });

export const calcDimensions = ({ digit }) => {
  const baseWidth = 30;
  const baseHeight = 55;

  switch (digit) {
    case ZERO:
      return dimTpl({ w: baseWidth, h: baseHeight });
    case ONE:
      return dimTpl({ w: baseWidth, h: baseHeight * 2 });
    case TWO:
      return dimTpl({ w: baseWidth, h: baseHeight * 3 });
    case THREE:
      return dimTpl({ w: baseWidth, h: baseHeight * 4 });
    case FOUR:
      return dimTpl({ w: baseWidth * 2, h: baseHeight });
    case FIVE:
      return dimTpl({ w: baseWidth * 2, h: baseHeight * 2 });
    case SIX:
      return dimTpl({ w: baseWidth * 2, h: baseHeight * 3 });
    case SEVEN:
      return dimTpl({ w: baseWidth * 2, h: baseHeight * 4 });
    case EIGHT:
      return dimTpl({ w: baseHeight * 2, h: baseWidth * 2 });
    case NINE:
      return dimTpl({ w: baseHeight * 2, h: baseHeight * 2 });
    default:
      return null;
  }
};

export const Digit = ({
  primaryColor,
  secondaryColor,
  digit,
  isCheckSum,
  hideNumbers
}) => {
  const { container } = styles;
  const inlineStyles = {
    color: isCheckSum ? primaryColor : secondaryColor,
    backgroundColor: isCheckSum ? secondaryColor : primaryColor,
    border: `1px solid ${primaryColor}`,
    ...calcDimensions({ digit })
  };
  return (
    <div style={inlineStyles} className={container}>{!hideNumbers && digit}</div>
  );
};

Digit.propTypes = {
  primaryColor: PropTypes.string.isRequired,
  secondaryColor: PropTypes.string.isRequired,
  digit: PropTypes.number,
  isCheckSum: PropTypes.bool
};

Digit.defaultProps = {
  isCheckSum: false,
  hideNumbers: false
};
