import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import {
  Digit,
  dimTpl,
  calcDimensions
} from './index';

describe('Digit', () => {
  const props = {
    primaryColor: '#ffff11',
    secondaryColor: '#11ffff',
    digit: 1
  };

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Digit {...props}/>, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('shows digit', () => {
    const component = shallow(<Digit {...props}/>);
    expect(component.text()).toBe('1');
  });

  it('hides digit', () => {
    const newProps = { ...props, hideNumbers: true };
    const component = shallow(<Digit {...newProps}/>);
    expect(component.text()).toBe('');
  });

  it('changes styles if it is checkSum', () => {
    const newProps = { ...props, isCheckSum: true };
    const component = shallow(<Digit {...newProps}/>);
    const { color, backgroundColor } = component.prop('style');
    expect(color).toBe(props.primaryColor);
    expect(backgroundColor).toBe(props.secondaryColor);
  });

  it('uses default styles if it is NOT checkSum', () => {
    const newProps = { ...props, isCheckSum: false };
    const component = shallow(<Digit {...newProps}/>);
    const { color, backgroundColor } = component.prop('style');
    expect(backgroundColor).toBe(props.primaryColor);
    expect(color).toBe(props.secondaryColor);
  });

  test('dimTpl', () => {
    const w = 10;
    const h = 20;
    const result = dimTpl({ w, h });
    expect(Object.keys(result).length).toBe(2);
    expect(result.width).toBeDefined();
    expect(result.height).toBeDefined();
    expect(result.width).toBe(`${w}px`);
    expect(result.height).toBe(`${h}px`);
  });

  describe('calcDimensions', () => {
    const baseWidth = 30;
    const baseHeight = 55;
    const digits = [
      { digit: 0, width: baseWidth, height: baseHeight },
      { digit: 1, width: baseWidth, height: baseHeight * 2 },
      { digit: 2, width: baseWidth, height: baseHeight * 3 },
      { digit: 3, width: baseWidth, height: baseHeight * 4 },
      { digit: 4, width: baseWidth * 2, height: baseHeight },
      { digit: 5, width: baseWidth * 2, height: baseHeight * 2 },
      { digit: 6, width: baseWidth * 2, height: baseHeight * 3 },
      { digit: 7, width: baseWidth * 2, height: baseHeight * 4 },
      { digit: 8, width: baseHeight * 2, height: baseWidth * 2 },
      { digit: 9, width: baseHeight * 2, height: baseHeight * 2 }
    ];

    digits.forEach(({ digit, width, height }) => {
      test(`correct calculations for: ${digit}`, () => {
        const result = calcDimensions({ digit });
        expect(Object.keys(result).length).toBe(2);
        expect(result.width).toBeDefined();
        expect(result.height).toBeDefined();
        expect(result.width).toBe(`${width}px`);
        expect(result.height).toBe(`${height}px`);
      });
    });

    test('renders null on no digit', () => {
      const result = calcDimensions({ digit: undefined });
      expect(result).toBe(null);
    });
  });
});
