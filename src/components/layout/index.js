import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';

export const Layout = ({ children }) => {
  return (
    <div>{children}</div>
  );
};
