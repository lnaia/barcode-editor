import { shallow } from 'enzyme/build';
import React from 'react';
import ReactDOM from 'react-dom';
import { Layout } from './index';

describe('Header', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Layout/>, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('renders children', () => {
    const component = shallow(<Layout>Sample Text</Layout>);
    expect(component.text()).toBe('Sample Text');
  })
});
