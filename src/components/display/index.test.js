import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import { Display } from './index';

describe('Display', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    const props = {
      primaryColor: '#ff1111',
      secondaryColor: '#1122ff'
    };
    ReactDOM.render(<Display {...props}/>, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('show dependent components', () => {
    const props = {
      digits: [0],
      primaryColor: '#ff1111',
      secondaryColor: '#1122ff'
    };
    const component = shallow(<Display {...props}/>);
    const digits = component.find('Digit');
    expect(digits.length).toBe(2);
  });

  it('contain common attrs', () => {
    const props = {
      digits: [0],
      primaryColor: '#ff1111',
      secondaryColor: '#1122ff'
    };
    const component = shallow(<Display {...props}/>);
    const digits = component.find('Digit');

    expect(digits.at(0).prop('primaryColor')).toBe(props.primaryColor);
    expect(digits.at(0).prop('secondaryColor')).toBe(props.secondaryColor);
    expect(digits.at(0).prop('digit')).toBe(0);
    expect(digits.at(0).prop('hideNumbers')).toBe(false);
  });

  it('shows a warning when there are no digits to display', () => {
    const props = {
      digits: [],
      primaryColor: '#ff1111',
      secondaryColor: '#1122ff'
    };
    const component = shallow(<Display {...props}/>);
    const digits = component.find('Digit');
    expect(digits.length).toBe(0);
    expect(component.text()).toBe('No digits to display - please type a number.');
  });


  it('last digit is checkSum', () => {
    const props = {
      digits: [0],
      primaryColor: '#ff1111',
      secondaryColor: '#1122ff'
    };
    const component = shallow(<Display {...props}/>);
    const value = component.find('Digit').at(1).prop('isCheckSum');
    expect(value).toBe(true);
  });
});
