import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import { calculate } from '../../utils/checksum';
import { Digit } from '../digit';
import styles from './index.module.css';

export const Display = ({ digits, primaryColor, secondaryColor, hideNumbers }) => {
  const { empty } = styles;
  const emptyClass = classNames(
    'alert',
    'alert-info',
    empty
  );

  if (digits.length === 0) {
    return (
      <div className={emptyClass} role="alert">
        No digits to display - please type a number.
      </div>
    );
  }

  const genProps = (digit, keySuffix) => ({
    key: `digit-${digit}-${keySuffix}`,
    primaryColor,
    secondaryColor,
    digit,
    hideNumbers
  });

  const { container } = styles;
  return (
    <div className={container}>
      {digits.map((digit, i) => <Digit {...genProps(digit, i)}/>)}
      <Digit isCheckSum={true} {...genProps(calculate({ digits }), 'checksum')}/>
    </div>
  );
};

Display.propTypes = {
  digits: PropTypes.arrayOf(PropTypes.number),
  primaryColor: PropTypes.string.isRequired,
  secondaryColor: PropTypes.string.isRequired,
  hideNumbers: PropTypes.bool
};

Display.defaultProps = {
  digits: []
};
